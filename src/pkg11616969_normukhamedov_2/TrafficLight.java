/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg11616969_normukhamedov_2;

import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author Helkern
 */
public class TrafficLight extends Thread {
    //Creating new instances of circle shapes and other shared variables
    Circle cRed = new Circle(50, Color.RED);
    Circle cYellow = new Circle(50, Color.GREY);
    Circle cGreen = new Circle(50, Color.GREY);
    Boolean execute = false;
    String valRed;
    String valYellow;
    String valGreen;
    

    public StackPane T() {
        //Creating rectange shape 
        Rectangle rectangle = new Rectangle();

        //Setting properties 
        rectangle.setWidth(350.0f);
        rectangle.setHeight(150.0f);

        //Rounding edges 
        rectangle.setArcWidth(90.0);
        rectangle.setArcHeight(90.0);
        //horizontal boxes
        HBox h = new HBox(10);
        HBox h2 = new HBox(10);
        h.toFront();
        rectangle.toBack();
        h.getChildren().addAll(cRed, cYellow, cGreen); //appending to the horizontal box circles
        h2.getChildren().add(rectangle);
        h.setAlignment(Pos.CENTER);
        h2.setAlignment(Pos.CENTER);
        GridPane g = new GridPane();
        Label lblRed = new Label("Red");
        TextField txtRed = new TextField("3");
        Label lblYellow = new Label("Yellow");
        TextField txtYellow = new TextField("3");
        Label lblGreen = new Label("Green");
        TextField txtGreen = new TextField("3");
        //Custom validation for textfields on text change event, doesn't allow to insert other than digits
        txtRed.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!newValue.matches("\\d*")) {
                txtRed.setText(newValue.replaceAll("[^\\d]", ""));
            }
            try {
                if (!(Integer.valueOf(newValue) >= 0 && Integer.valueOf(newValue) <= 100)) {
                    txtRed.setText(newValue.replaceAll(newValue, oldValue));
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        });
        txtYellow.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!newValue.matches("\\d*")) {
                txtYellow.setText(newValue.replaceAll("[^\\d]", ""));
            }
            try {
                if (!(Integer.valueOf(newValue) >= 0 && Integer.valueOf(newValue) <= 100)) {
                    txtYellow.setText(newValue.replaceAll(newValue, oldValue));
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        });
        txtGreen.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!newValue.matches("\\d*")) {
                txtGreen.setText(newValue.replaceAll("[^\\d]", ""));
            }
            try {
                if (!(Integer.valueOf(newValue) >= 0 && Integer.valueOf(newValue) <= 100)) {
                    txtGreen.setText(newValue.replaceAll(newValue, oldValue));
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        });
        
        
        
        //Setting elements to specific column for Grid pane
        g.add(lblRed, 0, 0);
        g.add(txtRed, 1, 0);
        g.add(lblYellow, 0, 1);
        g.add(txtYellow, 1, 1);
        g.add(lblGreen, 0, 2);
        g.add(txtGreen, 1, 2);
        g.setAlignment(Pos.BOTTOM_LEFT);
        Thread tr = new Thread(this);
        Button btnStart = new Button("Start");
        Button btnStop = new Button("Stop");
        btnStart.setOnAction((event) -> {//Event on button start, switches on the loop on run() method, gets seconds and saves to variables for sleep thread
            execute = true;
            valRed = txtRed.getText() + "000";
            valYellow = txtYellow.getText() + "000";
            valGreen = txtGreen.getText() + "000";
            try{
                tr.start();
            }
            catch(Exception e){
                System.out.println("Getting exception on thread start");
            }
        });
        btnStop.setOnAction((event) -> {
            execute = false;
            cRed.setFill(Color.GREY);
            cYellow.setFill(Color.GREY);
            cGreen.setFill(Color.GREY);
        });
        HBox hButtons = new HBox();
        hButtons.setSpacing(50);
        hButtons.setPadding(new Insets(0, 50, 30, 0));

        hButtons.getChildren().addAll(btnStart, btnStop);
        hButtons.setAlignment(Pos.BOTTOM_RIGHT);
        g.setPadding(new Insets(0, 0, 30, 50));
        // Finally adding all above elements to the stackpane and returning Stackpane
        StackPane root = new StackPane();
        root.setMaxWidth(500);
        root.setMaxHeight(400);
        root.getChildren().addAll(h2, h, g, hButtons);
        return root;
    }

    public void run() {
        try {
            while (execute) {
                //Loop sets one by one colors for the trafficlight with a specific sleep time delay between each of the circles
                cRed.setFill(Color.RED);
                cYellow.setFill(Color.GREY);
                cGreen.setFill(Color.GREY);
                Thread.sleep(Integer.parseInt(valRed));
                if (!execute) {break;}
                cYellow.setFill(Color.YELLOW);
                cRed.setFill(Color.GREY);
                cGreen.setFill(Color.GREY);
                Thread.sleep(Integer.parseInt(valYellow));
                if (!execute) {break;}
                cGreen.setFill(Color.GREEN);
                cRed.setFill(Color.GREY);
                cYellow.setFill(Color.GREY);
                Thread.sleep(Integer.parseInt(valGreen));
                if (!execute) {break;}
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(TrafficLight.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
        
    }

}
