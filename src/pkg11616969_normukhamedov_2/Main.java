/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg11616969_normukhamedov_2;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Helkern
 */
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        
        TrafficLight t = new TrafficLight(); // New instance of trafficlight class
        Thread tr = new Thread(t);
        tr.start();
        Scene scene = new Scene(t.T(), 500, 400); //scene add stackpane and set window size
        primaryStage.setTitle("Traffic light demo");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
